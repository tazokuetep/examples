use async_std::task;
use futures::future::join_all;
use std::time::Duration;

fn hello_world() {
    for i in 0..5 {
        println!("Hello, world!: {}", i);
        std::thread::sleep(Duration::from_millis(500));
    }
}

async fn async_hello_world(num: usize) {
    println!("Hello, world!: {}", num);
    task::sleep(Duration::from_millis(500)).await;
}

fn main() {
    let now = std::time::Instant::now();
    hello_world();
    println!("Required time for hello_world: {:?}", now.elapsed());

    let runtime = tokio::runtime::Builder::new_current_thread()
        .enable_all()
        .build()
        .unwrap();

    let futures = (0..5).map(|i| tokio::spawn(async_hello_world(i)));

    let now = std::time::Instant::now();
    runtime.block_on(async {
        join_all(futures).await;
    });
    println!("Required time for async_hello_world: {:?}", now.elapsed());
}
