
#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>

#define CHAIRS 5

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
sem_t barbers;			// customers without barber
sem_t customers;		// sleeping barbers
volatile int waiting = 0;	// waiting customers within the chair

void cut_hair();
void get_hair_cut(size_t id);

void* Barber(void* arg)
{
	while (waiting > 0) {
		sem_wait(&customers);
		pthread_mutex_lock(&mutex);
		waiting--;
		sem_post(&barbers);
		pthread_mutex_unlock(&mutex);
		cut_hair();
	}

	return NULL;
}

void* Customer(void* id)
{
	pthread_mutex_lock(&mutex);
	if (waiting < CHAIRS) {
		waiting ++;
		sem_post(&customers);
		pthread_mutex_unlock(&mutex);
		sem_wait(&barbers);
		get_hair_cut((size_t) id);
	} else
		pthread_mutex_unlock(&mutex);

}

void cut_hair() {
	size_t r = ((unsigned int)(((double) rand() / (double)RAND_MAX)*10))+1;
	printf("Barber cut hair\n");
	usleep(r*500);
}

void get_hair_cut(size_t id) {
	size_t r = ((unsigned int)(((double) rand() / (double)RAND_MAX)*10))+1;
	printf("Customer %zd get hair cut\n", id);
	usleep(r*500);
}

int main(int argc, char** argv) {
	pthread_t threads[CHAIRS];

	sem_init(&barbers, 0, 0);
	sem_init(&customers, 0, 0);

	// create customers
	for (size_t i = 0; i < CHAIRS; i++) {
		pthread_create(&(threads[i]), NULL, Customer, (void*)i);
 	}

	Barber(NULL);
	usleep(500);
	printf("Barber close shop\n");

	for (size_t i = 0; i < CHAIRS; i++) {
		pthread_join(threads[i], NULL);
 	}

	return 0;
}
