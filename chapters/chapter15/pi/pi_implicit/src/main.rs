use std::sync::mpsc;
use std::thread;
const NTHREADS: u64 = 2;
const NUM_STEPS: u64 = 100000000;

fn term(start: u64, end: u64) -> f64 {
    let step = 1.0 / NUM_STEPS as f64;
    let mut sum = 0.0;

    for i in start..end {
        let x = (i as f64 + 0.5) * step;
        sum += 4.0 / (1.0 + x * x);
    }

    sum
}

fn main() {
    let (tx, rx) = mpsc::channel();
    let mut sum = 0.0f64;

    for id in 0..NTHREADS {
        let thread_tx = tx.clone();
        let start = (NUM_STEPS / NTHREADS) * id;
        let end = (NUM_STEPS / NTHREADS) * (id + 1);

        // Each thread will send its partial sum via the channel
        thread::spawn(move || {
            let partial_sum = term(start, end);
            thread_tx.send(partial_sum).unwrap();
        });
    }

    for _ in 0..NTHREADS {
        // The `recv` method picks a message from the channel
        // `recv` will block the current thread if there no messages available
        sum = sum + rx.recv().unwrap();
    }

    println!("Pi: {}", sum * (1.0 / NUM_STEPS as f64));
}
