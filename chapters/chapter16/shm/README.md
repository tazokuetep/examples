# POSIX Shared Memory Example

The original example is distributed through http://www.os-book.com/ and revised by [Don Eric Heller](https://www.cse.psu.edu/~deh25/personal/vita.html).

* Compile demo on Linux and macOS: make
* Run demo: make test

Output on a Linux system:

```
% ls -l /dev/shm/shm-example
ls: cannot access /dev/shm/shm-example: No such file or directory

% ./shm-posix-producer
display: ./prod
00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00

display: prod
53 74 75 64 79 69 6e 67 20 4f 70 65 72 61 74 69
6e 67 20 53 79 73 74 65 6d 73 20 49 73 20 46 75
6e 21 0a 00 00 00 00 00 00 00 00 00 00 00 00 00
00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00

% ls -l /dev/shm/shm-example
-rw------- 1 dheller fcse 4096 Jan 31 15:29 /dev/shm/shm-example

% cat /dev/shm/shm-example
Studying Operating Systems Is Fun!

% ./shm-posix-producer
display: cons
53 74 75 64 79 69 6e 67 20 4f 70 65 72 61 74 69
6e 67 20 53 79 73 74 65 6d 73 20 49 73 20 46 75
6e 21 0a 00 00 00 00 00 00 00 00 00 00 00 00 00
00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00

Studying Operating Systems Is Fun!

% ls -l /dev/shm/shm-example
ls: cannot access /dev/shm/shm-example: No such file or directory
```
