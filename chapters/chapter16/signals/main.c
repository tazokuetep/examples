#define _POSIX_SOURCE
#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>

void sighandler(int sig) {
	printf("Receive signal number %d\n", sig);
}

int main(int argc, char** argv) {
	struct sigaction     	sUSR;

	/* define action for SIGINT */
	sUSR.sa_handler = sighandler;
	sUSR.sa_flags = 0;
	if (sigaction(SIGUSR1, &sUSR, NULL) < 0) {
		fprintf(stderr, "Error: sigaction\n");
		_exit(1);
	}

	/* send a signal to myself */
	kill(getpid(), SIGUSR1);

	sleep(1);

	printf("Terminate test program.\n");

	return 0;
}
