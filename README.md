# Foundations of Computer Science 3

This repository contains example code, which is presentedt with the course _Foundations of Computer Science 3 - Operating Systems and System Security"_ (Grundgebiete der Informatik 3 - Betriebssysteme und Systemsicherheit) at the RWTH Aachen University, Germany.

Take a look at https://os.rwth-aachen.de for details.
